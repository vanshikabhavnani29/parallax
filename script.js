window.onscroll = function(){
    var head = document.getElementById("header");
    if(document.body.scrollTop > 100 || document.documentElement.scrollTop > 100){
        head.className = "animate-on-scroll" + " animate-color";
        
    }else{
        head.className = head.className.replace("animate-on-scroll animate-color", "");
    }
}